import com.jcabi.aspects.RetryOnFailure;
import com.jcabi.aspects.Tv;
import com.jcabi.log.Logger;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.cactoos.io.LengthOf;
import org.cactoos.io.TeeInput;
import org.cactoos.text.TextOf;

@ToString
@EqualsAndHashCode(of = { "key" }, callSuper = true)
@SuppressWarnings("PMD.TooManyMethods")
public final class Ssh extends AbstractSshShell {

    public static final int PORT = 22;

    private static final String UTF_8 = "UTF-8";

    private final transient String key;

    private final transient String passphrase;

    public Ssh(final String adr, final String user, final URL priv)
        throws IOException {
        this(adr, Ssh.PORT, user, priv);
    }

    public Ssh(final InetAddress adr, final String user, final URL priv)
        throws IOException {
        this(adr, Ssh.PORT, user, priv);
    }

    public Ssh(final String adr, final String user, final String priv)
        throws UnknownHostException {
        this(adr, Ssh.PORT, user, priv);
    }

    public Ssh(final InetAddress adr, final String user, final String priv)
        throws UnknownHostException {
        this(adr.getCanonicalHostName(), Ssh.PORT, user, priv);
    }

    public Ssh(final String adr, final int prt,
        final String user, final URL priv) throws IOException {
        this(adr, prt, user, new TextOf(priv).asString());
    }

    public Ssh(final InetAddress adr, final int prt,
        final String user, final URL priv) throws IOException {
        this(
            adr.getCanonicalHostName(), prt, user,
            new TextOf(priv).asString()
        );
    }

    public Ssh(final String adr, final int prt,
        final String user, final String priv) throws UnknownHostException {
        this(adr, prt, user, priv, null);
    }

    public Ssh(final String adr, final int prt,
        final String user, final String priv,
        final String passphrs
    ) throws UnknownHostException {
        super(adr, prt, user);
        this.key = priv;
        this.passphrase = passphrs;
    }

    @SuppressWarnings("PMD.ProhibitPublicStaticMethods")
    public static String escape(final String arg) {
        return String.format("'%s'", arg.replace("'", "'\\''"));
    }

    @Override
    @RetryOnFailure(
        attempts = Tv.SEVEN,
        delay = 1,
        unit = TimeUnit.MINUTES,
        verbose = false,
        randomize = true,
        types = IOException.class
    )
    protected Session session() throws IOException {
        final File file = File.createTempFile("jcabi-ssh", ".key");
        try {
            JSch.setConfig("StrictHostKeyChecking", "no");
            JSch.setLogger(new JschLogger());
            final JSch jsch = new JSch();
            new LengthOf(
                new TeeInput(
                    this.key.replaceAll("\r", "")
                        .replaceAll("\n\\s+|\n{2,}", "\n")
                        .trim(),
                    file
                )
            ).value();
            jsch.setHostKeyRepository(new EasyRepo());
            if (this.passphrase == null) {
                jsch.addIdentity(file.getAbsolutePath());
            } else {
                jsch.addIdentity(
                    this.getLogin(),
                    this.key.getBytes(Ssh.UTF_8),
                    null,
                    this.passphrase.getBytes(Ssh.UTF_8)
                );
            }
            Logger.debug(
                this,
                "Opening SSH session to %s@%s:%s (%d bytes in RSA key)...",
                this.getLogin(), this.getAddr(), this.getPort(),
                file.length()
            );
            final Session session = jsch.getSession(
                this.getLogin(), this.getAddr(), this.getPort()
            );
            session.setServerAliveInterval(
                (int) TimeUnit.SECONDS.toMillis(Tv.TEN)
            );
            session.setServerAliveCountMax(Tv.MILLION);
            session.connect();
            return session;
        } catch (final JSchException ex) {
            throw new IOException(ex);
        } finally {
            Files.deleteIfExists(file.toPath());
        }
    }
}
