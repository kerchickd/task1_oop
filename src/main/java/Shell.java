import com.jcabi.aspects.Immutable;
import com.jcabi.log.Logger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.cactoos.io.DeadInput;
import org.cactoos.io.TeeOutputStream;

@Immutable
public interface Shell {

    int exec(String command, InputStream stdin,
        OutputStream stdout, OutputStream stderr) throws IOException;

    @Immutable
    @ToString
    @EqualsAndHashCode(of = "code")
    final class Fake implements Shell {

        private final int code;

        private final byte[] stdout;

        private final byte[] stderr;

        public Fake() {
            this(0, "", "");
        }

        public Fake(final int exit, final String out, final String err) {
            this(exit, out.getBytes(), err.getBytes());
        }

        public Fake(final int exit, final byte[] out, final byte[] err) {
            this.code = exit;
            this.stdout = out;
            this.stderr = err;
        }
        @Override
        public int exec(final String command, final InputStream stdin,
            final OutputStream sout, final OutputStream serr)
            throws IOException {
            while (true) {
                if (stdin.read(new byte[2048]) < 0) {
                    break;
                }
            }
            sout.write(this.stdout);
            sout.close();
            serr.write(this.stderr);
            serr.close();
            return this.code;
        }
    }


    @Immutable
    @ToString
    @EqualsAndHashCode(of = "origin")
    final class Safe implements Shell {

        private final transient Shell origin;

        public Safe(final Shell shell) {
            this.origin = shell;
        }

        @Override
        public int exec(final String command, final InputStream stdin,
            final OutputStream stdout, final OutputStream stderr)
            throws IOException {
            final int exit = this.origin.exec(command, stdin, stdout, stderr);
            if (exit != 0) {
                throw new IllegalArgumentException(
                    String.format("non-zero exit code #%d: %s", exit, command)
                );
            }
            return exit;
        }
    }

    @Immutable
    @ToString
    @EqualsAndHashCode(of = "origin")
    final class Empty {

        private final transient Shell origin;

        public Empty(final Shell shell) {
            this.origin = shell;
        }

        public int exec(final String cmd) throws IOException {
            return this.origin.exec(
                cmd, new DeadInput().stream(),
                Logger.stream(Level.INFO, this),
                Logger.stream(Level.WARNING, this)
            );
        }
    }

    @Immutable
    @ToString
    @EqualsAndHashCode(of = "origin")
    final class Plain {

        private final transient Shell origin;

        public Plain(final Shell shell) {
            this.origin = shell;
        }

        public String exec(final String cmd) throws IOException {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.origin.exec(
                cmd, new DeadInput().stream(),
                baos, baos
            );
            return baos.toString(StandardCharsets.UTF_8.toString());
        }
    }

    @Immutable
    @ToString
    @EqualsAndHashCode(of = "orgn")
    final class Verbose implements Shell {

        private final transient Shell orgn;

        public Verbose(final Shell shell) {
            this.orgn = shell;
        }

        @Override
        public int exec(final String command, final InputStream stdin,
            final OutputStream stdout, final OutputStream stderr)
            throws IOException {
            return this.orgn.exec(
                command, stdin,
                new TeeOutputStream(stdout, Logger.stream(Level.INFO, this)),
                new TeeOutputStream(stderr, Logger.stream(Level.WARNING, this))
            );
        }
    }
}
