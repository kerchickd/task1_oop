import com.jcabi.log.Logger;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

interface Execution {

    int exec() throws IOException;

    final class Default implements Execution {

        private final transient String command;

        private final transient InputStream stdin;

        private final transient OutputStream stdout;

        private final transient OutputStream stderr;

        private final transient Session session;


        Default(final String cmd, final InputStream input,
            final OutputStream out, final OutputStream err,
            final Session sess) {
            this.command = cmd;
            this.stdin = input;
            this.stdout = out;
            this.stderr = err;
            this.session = sess;
        }

        @Override
        public int exec() throws IOException {
            try {
                final ChannelExec channel = ChannelExec.class.cast(
                    this.session.openChannel("exec")
                );
                channel.setErrStream(this.stderr, false);
                channel.setOutputStream(this.stdout, false);
                channel.setInputStream(this.stdin, false);
                channel.setCommand(this.command);
                channel.connect();
                Logger.info(this, "$ %s", this.command);
                return this.exec(channel);
            } catch (final JSchException ex) {
                throw new IOException(ex);
            } finally {
                this.session.disconnect();
            }
        }

        private int exec(final ChannelExec channel) throws IOException {
            try {
                return this.code(channel);
            } finally {
                channel.disconnect();
            }
        }


        @SuppressWarnings("PMD.AvoidCatchingGenericException")
        private int code(final ChannelExec exec) throws IOException {
            while (!exec.isClosed()) {
                try {
                    this.session.sendKeepAliveMsg();
                } catch (final Exception ex) {
                    throw new IOException(ex);
                }
                try {
                    TimeUnit.SECONDS.sleep(1L);
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    throw new IOException(ex);
                }
            }
            return exec.getExitStatus();
        }
    }
}
