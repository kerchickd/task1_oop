package mock;

import org.apache.sshd.server.Command;
import org.apache.sshd.server.CommandFactory;

public final class MkCommandCreator implements CommandFactory {
    @Override
    public Command createCommand(final String command) {
        return new MkCommand(command);
    }
}
