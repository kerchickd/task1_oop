package mock;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;
import org.cactoos.io.LengthOf;
import org.cactoos.io.OutputTo;
import org.cactoos.io.TeeInput;


public final class MkCommand implements Command {

    private final transient String command;

    private transient ExitCallback callback;


    private transient OutputStream output;


    MkCommand(final String cmd) {
        this.command = cmd;
    }

    @Override
    public void setInputStream(final InputStream input) {
    }

    @Override
    public void setOutputStream(final OutputStream stream) {
        this.output = stream;
    }

    @Override
    public void setErrorStream(final OutputStream err) {

    }

    @Override
    public void setExitCallback(final ExitCallback cllbck) {
        this.callback = cllbck;
    }

    @Override
    public void start(final Environment env) throws IOException {
        new LengthOf(
            new TeeInput(this.command, new OutputTo(this.output))
        ).value();
        this.callback.onExit(0);
    }

    @Override
    public void destroy() {

    }
}
