import com.jcraft.jsch.Session;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(of = { "addr", "port", "login" })
@Getter
@SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
abstract class AbstractSshShell implements Shell {


    private final transient String addr;

    private final transient int port;

    private final transient String login;

    AbstractSshShell(
        final String adr,
        final int prt,
        final String user) throws UnknownHostException {
        this.addr = InetAddress.getByName(adr).getHostAddress();
        this.port = prt;
        this.login = user;
    }

    @Override
    public int exec(final String command, final InputStream stdin,
        final OutputStream stdout, final OutputStream stderr)
        throws IOException {
        return new Execution.Default(
            command,
            stdin,
            stdout,
            stderr,
            this.session()
        ).exec();
    }

    protected abstract Session session()  throws IOException;

}
