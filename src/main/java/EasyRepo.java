import com.jcraft.jsch.HostKey;
import com.jcraft.jsch.HostKeyRepository;
import com.jcraft.jsch.UserInfo;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
final class EasyRepo implements HostKeyRepository {

    @Override
    public int check(final String host, final byte[] bkey) {
        return HostKeyRepository.OK;
    }

    @Override
    public void add(final HostKey hostkey, final UserInfo info) {
    }

    @Override
    public void remove(final String host, final String type) {
    }

    @Override
    public void remove(final String host, final String type,
        final byte[] bkey) {

    }

    @Override
    public String getKnownHostsRepositoryID() {
        return "";
    }
    @Override
    public HostKey[] getHostKey() {
        return new HostKey[0];
    }

    @Override
    public HostKey[] getHostKey(final String host, final String type) {
        return new HostKey[0];
    }

}
